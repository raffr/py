from tkinter import * 
from tkinter import ttk
from tkinter import scrolledtext


root = Tk(className="SubCalcC")
root.configure(bg="#2e3440")
root.title = ("SubCalcC")
root_geoX = 520
root_geoY = 530 
root.geometry(f"{root_geoX}x{root_geoY}")

def subnetting():
	ipAddr = str(entry1.get())
	prefix = int(entry2.get())
	# Subnet Mask
	prefix -=24
	dec = [0,128,64,32,16,8,4,2,1]
	subnet_mask = sum(dec[0:prefix+1])

	#Binary
	binary = [] 
	limit = 8
	for i in range(prefix+24):
		while i == limit:
			binary.append(".")
			limit+=8

		binary.append("1")

	if entry2.get() == "24":
		if limit == 24:
			binary.append(".")
			

	for i in range(32-prefix-24):
		binary.append("0")

	binary = "".join(binary)
	

	# blok subnet
	blok_subnet = 256 - subnet_mask
	blok_subnet_list = [0]
	limit = 256/blok_subnet
	angka = 0
	for i in range(0,int(limit)):
		angka = angka + blok_subnet
		blok_subnet_list.append(angka)
	textOutput = f"""\n==============================================\n\n>>> IP Address\t\t= {ipAddr}\nSubnet Mask\t\t= 255.255.255.{subnet_mask}\nBinary\t\t= {binary}\nBlok Subnet\t\t= {blok_subnet_list}\n"""
	
	
	text_box1.insert("end",textOutput)
	# IP addr
	###pick ip oct 4
	ipAddr_rev = ipAddr[::-1]
	ip_list = []
	for i in range(len(ipAddr_rev)):
		ip_list.append(ipAddr_rev[i])
		if(ipAddr_rev[i]=="."):
			ip_list.remove(".")
			break
	ip_list = ip_list[::-1]
	ip_lastOct = int(''.join(ip_list))
	###find network addr, broadcast, last ip,first ip 
	i = 0
	ip_broadcast = 0 
	ip_end = 0
	ip_network = 0
	ip_start = 0
	while True:
		if(ip_lastOct<blok_subnet_list[i]):
			ip_broadcast += blok_subnet_list[i]
			ip_broadcast -= 1
			ip_end = ip_end + ip_broadcast - 1
			ip_network += blok_subnet_list[i-1]
			ip_start += ip_network+1
			break
		i+=1

	# IP network declaration 192.168.10. ...
	ip_body = ipAddr[::-1]
	ip_body = list(ip_body)
	i = 0
	for i in range(len(ip_body)):
		del(ip_body[0])
		if(ip_body[0] == "."):
			break
	ip_body = "".join(ip_body)
	ip_body = ip_body[::-1]

	# Output insert IP network,start,end,broadcast
	output_ipRange = f"""
IP Network\t\t= {ip_body}{ip_network}
IP Start\t\t= {ip_body}{ip_start}
IP End\t\t= {ip_body}{ip_end}
IP Broadcast\t\t= {ip_body}{ip_broadcast}

==============================================

	"""

	text_box1.insert("end",output_ipRange)


# clear button
def delete_textbox():
	text_box1.delete("1.0","end-1c")



## Frame
#left frame
frame1 = Frame(root,height=800,width=500,bg="#3b4252")
frame1.pack(side=LEFT,padx=10,pady=10)
#fix grid frame
frame1.grid_propagate(0)

## Label
# label title IP
label_title = Label(frame1,text="IP Calc class C",bg="#3b4252",fg="#ffffff",font=("NotoSans 15"))
label_title.grid(column=0,row=0,sticky="w",padx=20,pady=10)

#label entry IP address
label_entry1 = Label(frame1,text="IP Address ",bg = "#3b4252",fg="#ffffff",font=("NotoRegular 11"))
label_entry1.grid(column=0,row=1,sticky="w",padx=20,pady=5)

#label entry Prefix
label_entry2 = Label(frame1,text=f"Prefix\t\t/",bg="#3b4252",fg="#ffffff",padx=20,pady=5,font=("NotoRegular 11"))
label_entry2.grid(column=0,row=2,sticky="w")

label_empty = Label(frame1,text="",bg="#3b4252")
label_empty.grid(column=0,row=5,padx=20,pady=10)

## Entry
entry1 = Entry(frame1,bg="#4c566a",bd=0,highlightthickness=1,fg="#ffffff",width=34)
entry1.configure(highlightbackground="#5e81ac",highlightcolor="#88c0d0")
entry1.grid(column=1,row=1,sticky="w")

entry2 = Entry(frame1,bg="#4c566a",bd=0,highlightthickness=1,fg="#ffffff",width=34)
entry2.configure(highlightbackground="#5e81ac",highlightcolor="#88c0d0")
entry2.grid(column=1,row=2,sticky="w")
#entry.get()
string_entry1 = entry1.get()
string_entry2 = entry2.get()

## Scrolled Text Box
text_box1 = scrolledtext.ScrolledText(frame1,bg="#3b4252",width=55,height=40,highlightthickness=1,bd=0,fg="#eceff4")
text_box1.configure(highlightbackground="#4c566a",highlightcolor="#8fbcbb")
text_box1.grid(columnspan=2,row=4,pady=5,padx=20,column=0)

## Button
#image button
img_button1=PhotoImage(file="image/startButton.png")
img_clear = PhotoImage(file="image/clearbutton.png")

button1_start = Button(frame1,image=img_button1,bg="#3b4252",bd=0,height=29,fg="#ffffff",highlightthickness=0,command=subnetting,activebackground="#3b4252")
button1_start.grid(column=1,row=3,pady=10,sticky="w")

button2_del = Button(frame1,text="CLEAR",bg="#3b4252",activebackground="#3b4252",image=img_clear,highlightthickness=0,bd=0,command=delete_textbox)
button2_del.grid(columnspan=2,row=5,sticky="w",padx=20)


## Resize window dynamically
Grid.rowconfigure(frame1,[4],weight=1)
Grid.rowconfigure(root,[0],weight=1)
Grid.columnconfigure(root,[0,1],weight=1)

#mainloop
root.mainloop()

