
# Subnet Mask

def subnet_mask():
	print(">>> subnet mask")
	prefix = int(input("Masukkan prefix : "))
	
	list_angka1 = []
	batas = 1
	limit = 8
	angka = 1	
	print("subnet mask = ",end="")

	while batas <= prefix:
		print(1,end = "")
		list_angka1.append(angka)
		while batas == limit:
			print(".",end = "")
			limit = limit + 8
		batas = batas + 1
	
	del list_angka1[0:24]
	data = [128,64,32,16,8,4,2,1]
	jumlah_angka1 = sum(list_angka1)
	hasil = 0
	urutan = 0
	for ulang in range(0,jumlah_angka1):
		hasil = hasil + data[urutan]
		urutan = urutan + 1

	print(end="")

	for loop in range(32-prefix):
		print("0", end = "")
	
	print("\n\t\t255","\t  255","\t  255","\t  ",hasil)

	print("\n")
	return

def jumlah_subnet():
	print(">>> Jumlah subnet")	
	prefix = int(input("masukkan prefix : "))
	
	prefix = prefix - 24
	jumlah_subnet.hasil = 2**prefix	
	print(f"jumlah subnet = {jumlah_subnet.hasil}\n")
	return jumlah_subnet.hasil

def host_per_subnet():
	print(">>> jumlah host per subnet ")
	prefix = int(input("Masukkan prefix :"))
	prefix = 32 - prefix
	
	host_subnet = 2**prefix - 2
	print("Jumlah host per subnet =",host_subnet,"\n")

def blok_subnet():
	print(">>> Blok Subnet")
	subnet = int(input("Masukkan subnet mask (oktet ke-4): "))
	jumlah_subnet()
	blok_subnet = 256-subnet

	print("Blok subnet =",blok_subnet,"\n")
	
	limit = 1
	jumlah_blok = 0
	list_hasil = [0]
	print("blok subnet = 0,", end = "")
	while limit < jumlah_subnet.hasil:
		jumlah_blok = jumlah_blok + blok_subnet
		limit += 1
		list_hasil.append(jumlah_blok)
		print(f"{jumlah_blok},",end="")
	
	print("\n")
	
	return list_hasil
	
	
def prefix():
	print(">>> Mencari prefix")
	subnet = int(input("Masukkan subnet mask (oktet ke-4) :"))
	
	list_bin = [128,64,32,16,8,4,2,1]
	batas = 0
	list_1 = []

	while subnet > 0:
		subnet = subnet - list_bin[batas]
		batas = batas + 1
		list_1.append(1)
	
	list_1_sum = list_1.count(1)
	prefix = 24 + list_1_sum
	print(f"Prefix = /{prefix}")

	jumlah = sum(list_1)
	list_0 = [0,0,0,0,0,0,0,0] 
	print(f"11111111.11111111.11111111.",end="")
	limit = 0
	for loop in range(batas):
		print(f"{list_1[0]}",end="")
		limit += 1
	del list_0[0:list_1_sum]
	str_0 = str(list_0)
	print(*list_0, sep="")
	print("\n")

def ip_a(list_hasil):
	oktet1 = int(input("ip address oktet ke-1 = "))
	oktet2 = int(input("ip address oktet ke-2 = "))
	oktet3 = int(input("ip address oktet ke-3 = "))
	oktet4 = int(input("ip address oktet ke-4 = "))

	# network
	limit =len(list_hasil) 
	print("\nNetwork Address :  ")
	for loop in range(limit):
		oktet4_network = list_hasil[loop]
		print(f"{loop}. {oktet1}.{oktet2}.{oktet3}.{oktet4_network}",)
	
	print()
	
	# ip_pertama =
	limit = len(list_hasil)
	ip_pertama = []
	start = 0
	for loop in range(limit):
		angka = list_hasil[start]
		ip_pertama.append(angka)
		start = start + 1
	x = 0
	for loop in range(limit):
		ip_pertama[x] = ip_pertama[x] + 1
		x = x + 1
	
	print("Host Pertama : ")
	for loop in range(limit):
		print(f"{loop}. {oktet1}.{oktet2}.{oktet3}.{ip_pertama[loop]}")
	print()	
	 
	# ip_terakhir
	limit = len(list_hasil)
	hasil_blok = []	
	for loop in range(limit):
		tambah = list_hasil[loop]
		hasil_blok.append(tambah)
	del hasil_blok[0]
	
	hasil_dikurangi = []
	for loop in range(limit-1):
		dikurangi = hasil_blok[loop] - 2
		hasil_dikurangi.append(dikurangi)
	
	print("Host Terakhir : ")
	for loop in range(limit-1):
		print(f"{loop}.{oktet1}.{oktet2}.{oktet3}.{hasil_dikurangi[loop]}")
		
	print()

	# broadcast = 
	limit = len(list_hasil)
	blok_subnet = []
	for loop in range(limit):
		hasil_loop = list_hasil[loop]
		blok_subnet.append(hasil_loop)
	del blok_subnet[0]
	hasil_blok = []
	for loop in range(limit-1):
		hasil_loop = blok_subnet[loop] - 1
		hasil_blok.append(hasil_loop)
	
	print("Broadcast : ")
	for loop in range(limit-1):
		print(f"{loop}. {oktet1}.{oktet2}.{oktet3}.{hasil_blok[loop]}")


def start(): 
	print()
	print("===============================================")
	print("======== Class C Subnetting Calculator ========")
	print("===============================================")
	print("1. Menghitung subnet mask")
	print("2. Menghitung jumlah subnet")
	print("3. Menghitung jumlah host per subnet")
	print("4. Blok Subnet")
	print("5. Mencari prefix dari jumlah subnet")
	print("6. Mencari Network, host pertama, host terakhir, broadcast")
	
	print()

	inp = int(input ("Pilih salah satu (angka) : "))

	print()

	if (inp == 1):
		subnet_mask()
	elif (inp == 2):
		jumlah_subnet()
	elif (inp == 3):
		host_per_subnet()
	elif (inp == 4):
		blok_subnet()
	elif (inp == 5):
		prefix()
	elif (inp == 6):
		list_hasil = blok_subnet()
		ip_a(list_hasil)
	else:
		print("input salah")


############################################

repeat = "n"

def loop():
	while True:
		start()
		ulang()	
		break
def ulang():
	globals()['repeat'] = input("kembali ke awal ? (y/n)\n")
	if repeat == "y":
		loop()
	elif repeat == "n":
		print("")
		print(">>> Exit")
		print("")

print("""
 _   _      _ _       _ 
| | | | ___| | | ___ | |
| |_| |/ _ \ | |/ _ \| |
|  _  |  __/ | | (_) |_|
|_| |_|\___|_|_|\___/(_)""")

start()
ulang()


